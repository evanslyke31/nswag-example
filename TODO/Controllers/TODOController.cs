﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TODO.Models;

namespace TODO.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TODOController : ControllerBase
    {
        private static List<TODOItem> todos;
        private readonly ILogger<TODOController> _logger;
        private bool isSet => todos?.Any() ?? false;

        public TODOController(ILogger<TODOController> logger)
        {
            _logger = logger;
            if(!isSet) {
                todos = new List<TODOItem>();
                todos.Add(new TODOItem { Name = "Get Groceries", IsCompleted = false });
                todos.Add(new TODOItem { Name = "Wash Car", IsCompleted = true });
                todos.Add(new TODOItem { Name = "Clean", IsCompleted = false });
            }
        }

        /// <summary>
        /// Create todo item
        /// </summary>
        /// <param name="name">Name of the todo item</param>
        /// <response code="200">Successful operation!</response>
        [HttpPost]
        [Route("{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult Create(string name)
        {
            todos.Add(new TODOItem { Name = name, IsCompleted = false });
            return Ok();
        }

        /// <summary>
        /// Gets todo itemss
        /// </summary>
        /// <response code="200">Successful operation!</response>
        [HttpGet]
        [ResponseCache(NoStore = true, Duration = 0)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<TODOItem>> Retrieve()
        {
            return todos;
        }

        /// <summary>
        /// Update todo item by name
        /// </summary>
        /// <param name="name">Name of the todo item</param>
        /// <param name="isCompleted">Whether it is completed or not</param>
        /// <response code="200">Successful operation!</response>
        /// <response code="404">Todo item does not exist</response>
        [HttpPut]
        [Route("{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult Update(string name, bool isCompleted)
        {
            var item = todos.FirstOrDefault(x => x.Name == name);
            if (item == null)
                return NotFound();
            item.IsCompleted = isCompleted;
            return Ok();
        }


        /// <summary>
        /// Delete todo item by name
        /// </summary>
        /// <param name="name">Name of the todo item</param>
        /// <response code="200">Successful operation!</response>
        /// <response code="404">Todo item does not exist</response>
        [HttpDelete]
        [Route("{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult Delete(string name)
        {
            var item = todos.FirstOrDefault(x => x.Name == name);
            if (item == null)
                return NotFound();
            todos.Remove(item);
            return Ok();
        }
    }
}
