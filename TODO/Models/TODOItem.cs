﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TODO.Models
{
    public class TODOItem
    {
        public string Name { get; set; }
        public bool IsCompleted { get; set; }
    }
}
